$( document ).ready(function() {

// Guillaume main.js
  $('#hidden_mode_recherche').hideseek({
  hidden_mode: true,
  list:'.hideseek-data',
  nodata: '',
  ignore_accents: true
  });

  $('.js-lazyYT').lazyYT();

// fin guillaume

  var styles;
  var Office_loc = [
      {
        lat: 48.882497,
        lon: 2.327707,
        // zoom: 11,
        title: 'Office de tourisme de France',
        icon: src='img/office_logo.svg',
        animation: google.maps.Animation.DROP,
        html: '<h3>Office de tourisme de France</h3>',

      }
  ];

  new Maplace({
    map_div: '#map',
    locations: Office_loc,
    start: 1,
    styles: styles,
    map_options: {
      zoom: 11
    },
    styles: {


      // 'Night': [{
      //     featureType: 'all',
      //     stylers: [
      //         { invert_lightness: 'true' }
      //     ]
      // }]
      "default":
      [
        {
          "featureType": "all",
          "elementType":"labels.text.fill",
          "stylers":[
            {"color":"#e55251"},
            {"visibility":"on"}]},
        {
          "featureType":"administrative",
          "elementType":"labels.text.fill",
          "stylers":[
            {"color":"#ec0b43"},
            {"weight":"0.01"},
            {"visibility":"on"}]},
        {
          "featureType":"administrative",
          "elementType":"labels.text.stroke",
          "stylers":[
            {"visibility":"on"},
            {"color":"#d3d8d9"}]},
        {
          "featureType":"administrative.province","elementType":"labels.text",
            "stylers":[
              {"color":"#ec0b43"},
              {"visibility":"off"}]},
        {
          "featureType":"administrative.province",
          "elementType":"labels.text.fill",
          "stylers":[
            {"weight":"0.01"},
            {"visibility":"off"}]},
        {
          "featureType":"administrative.province",
          "elementType":"labels.text.stroke",
          "stylers":[
            {"visibility":"simplified"}]},
        {
          "featureType":"administrative.neighborhood",
          "elementType":"geometry.fill",
          "stylers":[
            {"color":"#8abe87"}]},
        {
          "featureType":"administrative.land_parcel",
          "elementType":"geometry.fill",
          "stylers":[
            {"color":"#8abe87"}]},

        {
          "featureType":"landscape",
          "elementType":"all",
          "stylers":[
            {"color":"#d2d2d2"}]},
        {
          "featureType":"landscape.man_made",
          "elementType":"all",
          "stylers":[
            {"color":"#d3d8d9"},
            {"visibility":"on"}]},
        {
          "featureType":"landscape.natural",
          "elementType":"geometry.fill",
          "stylers":[
            {"visibility":"on"},{"weight":"1.40"},
            {"saturation":"-17"},
            {"lightness":"-36"},
            {"color":"#bec8cf"}]},
        {
          "featureType":"landscape.natural.landcover",
          "elementType":"geometry.fill",
          "stylers":[
            {"saturation":"-33"},
            {"visibility":"on"}]},
        {
          "featureType":"landscape.natural.terrain",
          "elementType":"labels.text.fill",
          "stylers":[
            {"color":"#a8c09b"}]},
        {
          "featureType":"poi",
          "elementType":"all",
          "stylers":[
            {"visibility":"off"}]},
        {
          "featureType":"poi.business",
          "elementType":"all",
          "stylers":[
            {"color":"#8a8a8a"},
            {"visibility":"off"}]},
        {
          "featureType":"poi.government",
          "elementType":"all",
          "stylers":[
            {"color":"#bfbfbf"},
            {"visibility":"on"}]},
        {
          "featureType":"poi.park",
          "elementType":"geometry.fill",
            "stylers":[
              {"color":"#8abe87"},
              {"visibility":"off"}]},
        {
          "featureType":"road",
          "elementType":"all",
            "stylers":[
              {"saturation":-100},
              {"lightness":"-8"}]},
        {
          "featureType":"road.highway",
          "elementType":"all",
            "stylers":[
              {"visibility":"off"}]},
        {
          "featureType":"road.arterial",
          "elementType":"labels.icon",
            "stylers":[{"visibility":"off"}]},
        {
          "featureType":"transit",
          "elementType":"all",
          "stylers":[
            {"visibility":"off"}]},
        {
          "featureType":"transit.station.bus",
          "elementType":"all",
          "stylers":[
            {"color":"#8a8a8a"},
            {"visibility":"off"}]},
        {
          "featureType":"water",
          "elementType":"all",
          "stylers":[
            {"color":"#46bcec"},
            {"visibility":"on"}]},
        {
          "featureType":"water",
          "elementType":"geometry.fill",
          "stylers":[
            {"color":"#3264b4"},
            {"lightness":"9"}]},
        {
          "featureType":"water",
          "elementType":"labels.text.fill",
          "stylers":[
            {"color":"#ffffff"}]}]

    }


  }).Load();
// resize = méthode
  $(window).resize(function(){
    location.reload();
  });
});
